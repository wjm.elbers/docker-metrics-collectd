#!/bin/bash

CONFIG_FILE="/etc/collectd/collectd.conf"

if [ "${GRAPHITE_HOST}" != "" ]; then
    sed -i "s/Host \"localhost\"/Host \"${GRAPHITE_HOST}\"/g" "${CONFIG_FILE}"
fi

if [ "${GRAPHITE_PORT}" != "" ]; then
    sed -i "s/Port \"2003\"/Port \"${GRAPHITE_PORT}\"/g" "${CONFIG_FILE}"
fi